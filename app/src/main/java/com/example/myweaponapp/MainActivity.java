package com.example.myweaponapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.myweaponapp.commondata.DataTransferClass;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button buttonAdd;
    ListView listViewWeapons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonAdd = findViewById(R.id.buttonAdd);
        listViewWeapons = findViewById(R.id.listViewWeapons);

        buttonAdd.setOnClickListener(buttonAddOnClick);
    }

    View.OnClickListener buttonAddOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent(getApplicationContext(), WeaponAddActivity.class);
            startActivityForResult(intent,1);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, DataTransferClass.WeaponList.GetWeaponsInStrings());

        listViewWeapons.setAdapter(adapter);
    }
}
