package com.example.myweaponapp.commondata;

import java.util.ArrayList;

public class WeaponsList
{
    private ArrayList<Weapon> weapons;

    public WeaponsList()
    {
        weapons = new ArrayList<>();
    }

    public void Add(Weapon weapon)
    {
        weapons.add(weapon);
    }

    public ArrayList<String> GetWeaponsInStrings()
    {
        ArrayList<String> strings = new ArrayList<>();

        for (int i = 0; i < weapons.size(); i++)
        {
            String output = "Name: "+weapons.get(i).Name+"\n"+
                    "DateOfProduction: "+weapons.get(i).DateOfProduction+"\n"+
                    "Material: "+weapons.get(i).Material+"\n"+
                    "Weight: "+weapons.get(i).Weight+"\n"+
                    "Price: "+weapons.get(i).Price+"$ \n";

            strings.add(output);
        }

        return strings;
    }
}
