package com.example.myweaponapp.commondata;

import java.util.Date;

public class Weapon
{
    public String Name;
    public Date DateOfProduction;
    public String Material;
    public int Weight;
    public int Price;

    public Weapon(String Name, Date DateOfProduction, String Material, int Weight, int Price)
    {
        this.Name = Name;
        this.DateOfProduction = DateOfProduction;
        this.Material = Material;
        this.Weight = Weight;
        this.Price = Price;
    }
}
