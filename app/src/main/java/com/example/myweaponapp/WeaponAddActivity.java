package com.example.myweaponapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;

import com.example.myweaponapp.commondata.DataTransferClass;
import com.example.myweaponapp.commondata.Weapon;

import java.util.Date;

public class WeaponAddActivity extends AppCompatActivity {

    Button buttonAddAndClose;
    CalendarView calendarViewDateOfProduction;
    EditText editTextName, editTextPrice;
    SeekBar seekBarWeight;
    Spinner spinnerMaterial;

    String[] material = new String[]{"Iron", "Steel", "Titanium", "Aluminum"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weapon_add);

        editTextName = findViewById(R.id.editTextName);
        editTextPrice = findViewById(R.id.editTextPrice);
        calendarViewDateOfProduction = findViewById(R.id.calendarViewDateOfProduction);
        seekBarWeight = findViewById(R.id.seekBarWeight);

        spinnerMaterial = findViewById(R.id.spinnerMaterial);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, material);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMaterial.setAdapter(spinnerAdapter);

        buttonAddAndClose = findViewById(R.id.buttonAddAndClose);
        buttonAddAndClose.setOnClickListener(buttonAddAndCloseOnClick);
    }

    View.OnClickListener buttonAddAndCloseOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
            String name = editTextName.getText().toString();
            Date date = new Date(calendarViewDateOfProduction.getDate());
            int weight = seekBarWeight.getProgress();
            int price = Integer.parseInt(editTextPrice.getText().toString());

            //First way

            String currentMaterial = "";

            int material = spinnerMaterial.getSelectedItemPosition();

            switch (material)
            {
                case 0:
                    currentMaterial = "Iron";
                    break;
                case 1:
                    currentMaterial = "Steel";
                    break;
                case 2:
                    currentMaterial = "Titanium";
                    break;
                case 3:
                    currentMaterial = "Aluminum";
                    break;
            }

            //Second way

            //String currentMaterial = spinnerMaterial.getItemAtPosition(material).toString();  //(Тоже работает короткий способ https://stackoverflow.com/questions/10331854/how-to-get-spinner-selected-item-value-to-string)

            DataTransferClass.WeaponList.Add(new Weapon(name, date, currentMaterial, weight, price));

            finish();
        }
    };
}
